package com.ks.pruebas;

import org.zeroturnaround.exec.ProcessExecutor;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class App
{
    public static void main(String[] args)
    {
        try
        {
            ProcessExecutor VLobjProceso = new ProcessExecutor();
            VLobjProceso.command("java", "-jar", "ip-1.0.jar");
            VLobjProceso.directory(new File("/opt/ks/tools/ip"));
            VLobjProceso.readOutput(true);

            VLobjProceso.environment("PATH", "$PATH:/usr/tandem/java64/bin");

            String VLstrRespuesta = VLobjProceso.execute().outputUTF8();
            System.out.println(VLstrRespuesta);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
